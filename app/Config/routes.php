<?php
Router::connect('/editor/*', array('controller' => 'pages', 'action' => 'editor'));
Router::connect('/game/*', array('controller' => 'pages', 'action' => 'display'));
CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';