editor = function() {
	var initialized			= false;
	var layers				= Object({'foreground' : undefined, 'background' : undefined, 'palette' : undefined});
	var dimensions			= Object({'width' : undefined, 'height' : undefined});
	var mapArr				= Object();
	var mapPalette			= new Image();
	var mapPos				= Object({'x' : 0, 'y' : 0});
	var mousePos			= Object({'x' : 0, 'y' : 0});
	var moving				= false;
	var placing				= false;
	var collisionPlacement	= false;
	var interactionCreation	= false;
	var shift				= Object({'state' : 0, 'pressed' : false, 'direction' : 0});
	var selectedTile		= Object({'x' : 0, 'y' : 0, 'w' : 0});
	var bg;
	var mapId				= null;

	this.init				= function(obj) {
		if(initialized)
			return;

		bg							= document.getElementById('background');
		pal							= document.getElementById('palette');
		layers.background			= bg.getContext('2d');
		layers.background.fillStyle	= 'rgba(255, 44, 44, 0.3)'; // Red transparent color for collision
		layers.palette				= pal.getContext('2d');
		dimensions.width			= parseInt(window.getComputedStyle(bg).width);
		dimensions.height			= parseInt(window.getComputedStyle(bg).height);

		mapPos.x					= -Math.floor(Math.floor(dimensions.width / 48) / 2);
		mapPos.y					= Math.floor(Math.floor(dimensions.height / 48) / 2);

		mapPalette.onload			= function() {
			document.getElementById('palette').style.height	= mapPalette.height + 'px';
			document.getElementById('palette').height		= mapPalette.height;
			drawPalette();
		}

		mapPalette.src				= '/resources/default48.bmp';
		bg.onmousedown				= layerMouseDown;
		window.onmouseup			= cancelInteraction;
		bg.onmousemove				= moveHandler;
		bg.onmouseleave				= cancelInteraction;
		pal.onclick					= paletteClick;
		window.onkeydown			= keyHandler;
		window.onkeyup				= keyHandler;
		startLoops();

		if(obj !== undefined) {
			if(obj['mapId'] !== undefined)
				mapId = obj['mapId'];
			if(obj['saveBtn'] !== undefined)
				document.getElementById(obj['saveBtn']).onclick = saveMap;
		}

		if(mapId != null)
			loadMap(mapId);

		bg.focus();
		initialized					= true;
	}

	var startLoops			= function() {
		setInterval(function() {
			drawMap();
		}, 40);
	}

	var addTile				= function(x, y, obj) { // Adds a tile to mapArr
		if(mapArr[y] === undefined)
			mapArr[y] = Object();

		if(obj !== undefined)
			obj			=	Object({
								'x' : obj['x'] !== undefined ? obj['x'] : selectedTile.x,
								'y' : obj['y'] !== undefined ? obj['x'] : selectedTile.y,
								'w' : obj['w'] !== undefined ? obj['x'] : false
							});
		else
			obj			= Object({'x' : selectedTile.x, 'y' : selectedTile.y, 'w' : false});

		mapArr[y][x] = obj;
	}

	var updateTile			= function(x, y, obj) { // Updates a tile in mapArr if it exists
		if(mapArr[y] === undefined || mapArr[y][x] === undefined || obj === undefined)
			return;

		if(obj['x'] !== undefined)
			mapArr[y][x]['x'] = obj['x'];

		if(obj['y'] !== undefined)
			mapArr[y][x]['y'] = obj['y'];

		if(obj['w'] !== undefined)
			mapArr[y][x]['w'] = obj['w'];

		if(obj['interaction'] !== undefined)
			mapArr[y][x]['interaction']	= obj['interaction'];
	}

	var layerMouseDown		= function(e) { // handler for tile placement, movement on the map and interaction creation
		mousePos.x = Math.abs(Math.floor(e.layerX / 48));
		mousePos.y = Math.abs(Math.floor(e.layerY / 48));

		if(e.buttons == 4)
			moving = true;

		if(e.buttons == 1) {
			moving	= false;

			if(interactionCreation) {
				var x = mapPos.x + mousePos.x;
				var y = mapPos.y - mousePos.y;
				var interaction	= prompt("Text", '');

				if(interaction.length > 0)
					updateTile(x, y, Object({'interaction' : interaction}));

				interactionCreation	= false;
				return;
			}

			placing	= true;
			
			if(!collisionPlacement)
				addTile(mapPos.x + mousePos.x, mapPos.y - mousePos.y);
			else
				updateTile(	mapPos.x + mousePos.x, mapPos.y - mousePos.y,
							Object({'w' : (	mapArr[mapPos.y - mousePos.y] !== undefined &&
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x] !== undefined && 
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x]['w']) ? false : true}
							));
		}
	}

	var cancelInteraction	= function(e) {
		moving		= false;
		placing		= false;
		mousePos	= Object({'x' : 0, 'y' : 0});
	}

	var moveHandler			= function(e) { // mouse move handler
		if(moving) { // map movement
			if(Math.abs(Math.floor(e.layerX / 48)) != mousePos.x) {
				newPos		=	Math.abs(Math.floor(e.layerX / 48));
				mapPos.x	+=	newPos > mousePos.x ? -1 : 1;
				mousePos.x	=	newPos;
			}

			if(Math.abs(Math.floor(e.layerY / 48)) != mousePos.y) {
				newPos		=	Math.abs(Math.floor(e.layerY / 48));
				mapPos.y	+=	newPos > mousePos.y ? 1 : -1;
				mousePos.y	=	newPos;
			}

			return;
		}

		if(placing) { // placing tiles / collision
			if(Math.abs(Math.floor(e.layerX / 48)) != mousePos.x && shift.direction != 'y') {
				newPos		=	Math.abs(Math.floor(e.layerX / 48));
				mousePos.x	=	newPos;

				if(collisionPlacement)
					updateTile(	mousePos.x + mapPos.x, mapPos.y - mousePos.y,
							Object({'w' : (	mapArr[mapPos.y - mousePos.y] !== undefined &&
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x] !== undefined && 
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x]['w']) ? false : true}
							));
				else
					addTile(newPos + mapPos.x, mapPos.y - mousePos.y);

				if(shift.pressed)
					shift.direction = 'x';
			}

			if(Math.abs(Math.floor(e.layerY / 48)) != mousePos.y && shift.direction != 'x') {
				newPos		=	Math.abs(Math.floor(e.layerY / 48));
				mousePos.y	=	newPos;

				if(collisionPlacement)
					updateTile(	mousePos.x + mapPos.x, mapPos.y - mousePos.y,
							Object({'w' : (	mapArr[mapPos.y - mousePos.y] !== undefined &&
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x] !== undefined && 
											mapArr[mapPos.y - mousePos.y][mapPos.x + mousePos.x]['w']) ? false : true}
							));
				else
					addTile(mousePos.x + mapPos.x, mapPos.y - newPos);

				if(shift.pressed)
					shift.direction = 'y';
			}

			return;
		}
	}

	var paletteClick		= function(e) { // tile selection
		var x	= Math.floor(e.layerX / 48);
		var y	= Math.floor(e.layerY / 48);

		selectedTile = Object({'x' : x, 'y' : y, 'w' : true});

		drawPalette();
		layers.palette.beginPath();
		layers.palette.rect(x * 48, y * 48, 48, 48);
		layers.palette.stroke();
	}

	var drawMap				= function() { // map rendering
		layers.background.clearRect(0, 0, dimensions.width, dimensions.height); // Clean-up
		for(var y = 0; y < dimensions.height / 48; y++) {
			for(var x = 0; x < dimensions.width / 48; x++) {
				layers.background.drawImage(mapPalette,
					(mapArr[-(y + -mapPos.y)] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x]['x'] !== undefined ? mapArr[-(y + -mapPos.y)][x + mapPos.x]['x'] : 0) * 48,
					(mapArr[-(y + -mapPos.y)] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x]['y'] !== undefined ? mapArr[-(y + -mapPos.y)][x + mapPos.x]['y'] : 0) * 48,
					48,						//unstretched palette width
					48,						//unstretched palette height
					x * 48,					//pos x on canvas
					y * 48,					//pos y on canvas
					48,						//size x (stretch)
					48						//size y (stretch)
				);

				// collision draw, the red transparent squares
				if(mapArr[-(y + -mapPos.y)] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x] !== undefined && mapArr[-(y + -mapPos.y)][x + mapPos.x]['w'] == true) {
					layers.background.beginPath();
					layers.background.rect(x * 48, y * 48, 48, 48);
					layers.background.fill();
				}
			}
		}
	}

	var drawPalette			= function() { // only required once, unless palette changes. Future stuff
		layers.palette.clearRect(0, 0, mapPalette.width, mapPalette.height);
		layers.palette.drawImage(mapPalette, 0, 0);
	}

	var keyHandler			= function(e) { // Interval keyhandler class. Better off internal after all
		if(e.which == 16) {
			shift.direction = 0;

			if(e.type == 'keydown')
				shift.pressed	= true;
			else {
				shift.pressed	= false;
				placing			= false;
			}
		}

		if(e.which == 17) {
			if(e.type == 'keydown') {
				collisionPlacement	= true
				moving				= false;
			} else {
				collisionPlacement	= false;
				placing				= false;
			}
		}

		if(e.which == 90) {
			if(e.type == 'keydown') {
				interactionCreation	= true;
			} else {
				interactionCreation	= false;
			}
		}
	}

	var loadMap			= function(mapId) { // Loads a map if the mapId var has been parsed into the init function
		if(mapId == null)
			mapId = '';

		var xml	=	new XMLHttpRequest();

		xml.onreadystatechange = function() {
			if (xml.readyState == 4 && xml.status == 200) {
				var temp = JSON.parse(xml.responseText);
				if(temp.success)
					mapArr = temp.data;
			}

			mapState = 'idle';
		}

		xml.open('get', '/pages/getMapById/' + mapId, false);
		xml.send();
	}

	var saveMap			= function() { // Map save, cakephp handles it all
		var xml	=	new XMLHttpRequest();

		xml.onreadystatechange = function() {
			if (xml.readyState == 4 && xml.status == 200) {
				var data = JSON.parse(xml.responseText);
				if(data.success) {
					if(confirm('Map opgeslagen, nu testen?'))
						window.location = '/game/' + data.data['map_id'];
					return;
				}

				alert('Er is iets misgegaan tijdens het opslaan');
			}
		}

		xml.open('POST', '/pages/saveMap/' + (mapId !== null ? mapId : ''), false);
		xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xml.send('_method=post&data[Map]=' + JSON.stringify(mapArr));
	}
}