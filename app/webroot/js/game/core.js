game = function() {
	var initialized		= false;
	var layers			= Object({'foreground' : undefined, 'background' : undefined, 'entities' : undefined});
	var dimensions		= Object({'width' : undefined, 'height' : undefined});
	var player			= Object({'x' : 0, 'y' : 0, 'moveY' : 0, 'moveX' : 0, 'state' : 'idle_s', 'ani' : Object(), 'walking' : false, 'prevAniState' : 1});
	var mapPalette		= new Image();
	var playerPalette	= new Image();
	var mapArr			= Object();
	var loop;
	var mapState;
	var drawInterval	= 40;
	var keyH			= new keyHandler();

	this.init			= function(options) { // Set variables, load palettes/spritesheets, set fps. load map, load player animation. starts the drawloops
		if(initialized)
			return;

		var bg				= document.getElementById('background');
		layers.background	= bg.getContext('2d');
		layers.entities		= document.getElementById('entities').getContext('2d');
		layers.foreground	= document.getElementById('foreground').getContext('2d');
		dimensions.width	= parseInt(window.getComputedStyle(bg).width);
		dimensions.height	= parseInt(window.getComputedStyle(bg).height);
		mapPalette.src		= '/resources/default48.bmp';
		playerPalette.src	= '/resources/player.png';

		if(typeof options == 'object') {
			if(options['fps'] !== undefined) {
				if(options['fps'] == 25)
					drawInterval = 40;
				else if(options['fps'] == 50)
					drawInterval = 20;
			}
		}

		loadMap(options.mapId !== undefined ? options.mapId : '');
		loadPlayerAni();
		startLoops();
		initialized			= true;
	}

	var startLoops		= function() { // Loops to render and walk/interact
		loop = setInterval(function() {
			drawMap();
			drawPlayer();
		}, drawInterval);

		setInterval(function() {
			if(keyH.getDirection() != "" && player.walking == false) {
				walk(keyH.getDirection());
				return;
			}

			if(keyH.keys()[90]) {
				interact(player.state.replace(/idle_/g, ''));
			}
		}, 20);
	}

	var interact		= function(direction) { // Interact with objects based on the way the player faces
		var nextPos		= Object({'x' : 0, 'y' : 0});
		switch(direction) {
			case 'n':
				nextPos.y = 1;
				break;
			case 's':
				nextPos.y = -1;
				break;
			case 'e':
				nextPos.x = 1;
				break;
			case 'w':
				nextPos.x = -1;
				break;
		}

		if(	mapArr[player.y + nextPos.y] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x]['interaction'] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x]['interaction'].length > 0) {
				alert(mapArr[player.y + nextPos.y][player.x + nextPos.x]['interaction']);
		}

		return;
	}

	var walk			= function(direction) { // Walk to the direction according to the key pressed
		player.state		= 'idle_' + direction;

		var nextPos		= Object({'x' : 0, 'y' : 0});
		switch(direction) {
			case 'n':
				nextPos.y = 1;
				break;
			case 's':
				nextPos.y = -1;
				break;
			case 'e':
				nextPos.x = 1;
				break;
			case 'w':
				nextPos.x = -1;
				break;
		}

		// Collision check
		if(	mapArr[player.y + nextPos.y] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x]['w'] !== undefined &&
			mapArr[player.y + nextPos.y][player.x + nextPos.x]['w'] == true)
			return;

		player.walking		= true;
		var walkAxis		= 'moveX';
		var moveAxis		= 'x';
		var axisDir			= 3;
		var wInt;

		if(direction == 'n' || direction == 's') {
			walkAxis	= 'moveY';
			moveAxis	= 'y';
		}

		if(direction == 'w' || direction == 's')
			axisDir		= -3;

		player.state = 'walk_' + direction + '_' + (player.prevAniState == 1 ? 2 : 1);
		player.prevAniState = player.prevAniState == 1 ? 2 : 1;

		wInt = setInterval(function() { // Walk 1 tile with an interval to give the player the feel of movement instead of just moving 1 tile instantly
			if(Math.abs(player[walkAxis]) == 36) {
				player.state = 'idle_' + direction;
			}

			if(Math.abs(player[walkAxis]) == 48) {
				player[walkAxis]	= 0;
				player[moveAxis]	+= axisDir / 3;
				player.walking		= false;
				player.state		= 'idle_' + direction;
				clearInterval(wInt);
				return;
			}


			player[walkAxis] 	+= axisDir;
			
		}, 17 * (keyH.keys()[16] == true ? 0.7 : 1));
	}

	var loadMap			= function(mapId) { // Load map from server by id. CakePHP creates a JSON object based on the data from MySQL via models/controllers
		if(mapId == null)
			mapId = '';

		mapState = 'loading';

		var xml	=	new XMLHttpRequest();

		xml.onreadystatechange = function() {
			if (xml.readyState == 4 && xml.status == 200) {
				var temp = JSON.parse(xml.responseText);
				if(temp.success)
					mapArr = temp.data;
			}

			mapState = 'idle';
		}

		xml.open('get', '/pages/getMapById/' + mapId, false);
		xml.send();
	}

	var loadPlayerAni	= function() { // Loads the json that contains the playerpalette positions. CakePHP loads a file on the server and converts it
		var xml	=	new XMLHttpRequest();

		xml.onreadystatechange = function() {
			if (xml.readyState == 4 && xml.status == 200) {
				var temp = JSON.parse(xml.responseText);
				if(temp.success)
					player.ani = temp.data;
			}
		}

		xml.open('get', '/pages/getPlayerAnimation', false);
		xml.send();
	}

	var drawMap			= function() { // Map render loop, called based on set fps with an interval
		layers.background.clearRect(0, 0, dimensions.width, dimensions.height); // Clean-up
		for(var y = -1; y < dimensions.height / 48 + 1; y++) {
			for(var x = -1; x < dimensions.width / 48 + 1; x++) {
				layers.background.drawImage(mapPalette,
					(mapArr[-(y + -player.y - 4)] !== undefined && mapArr[-(y + -player.y - 4)][x + player.x - 7] !== undefined && mapArr[-(y + -player.y - 4)][x + player.x - 7]['x'] !== undefined ? mapArr[-(y + -player.y - 4)][x + player.x - 7]['x'] : 0) * 48,
					(mapArr[-(y + -player.y - 4)] !== undefined && mapArr[-(y + -player.y - 4)][x + player.x - 7] !== undefined && mapArr[-(y + -player.y - 4)][x + player.x - 7]['y'] !== undefined ? mapArr[-(y + -player.y - 4)][x + player.x - 7]['y'] : 0) * 48,
					48,						//unstretched palette width
					48,						//unstretched palette height
					x * 48 + -player.moveX,	//pos x on canvas
					y * 48 + player.moveY,	//pos y on canvas
					48,						//size x (stretch)
					48						//size y (stretch)
				);
			}
		}
	}

	var drawPlayer		= function() { // Player render loop, called less often than drawMap yet just as important. renders the players
		layers.entities.clearRect(0, 0, dimensions.width, dimensions.height); // Clean-up
		layers.entities.drawImage(playerPalette,
			(player.ani[player.state] !== undefined && player.ani[player.state].x 		!== undefined ? player.ani[player.state].x 		: player.ani.default.x),
			(player.ani[player.state] !== undefined && player.ani[player.state].y 		!== undefined ? player.ani[player.state].y 		: player.ani.default.y),
			(player.ani[player.state] !== undefined && player.ani[player.state].width 	!== undefined ? player.ani[player.state].width 	: player.ani.default.width),
			(player.ani[player.state] !== undefined && player.ani[player.state].height 	!== undefined ? player.ani[player.state].height : player.ani.default.height),
			(player.ani[player.state] !== undefined && player.ani[player.state].pos_x 	!== undefined ? player.ani[player.state].pos_x 	: player.ani.default.pos_x) + 48 * 7,
			(player.ani[player.state] !== undefined && player.ani[player.state].pos_y 	!== undefined ? player.ani[player.state].pos_y 	: player.ani.default.pos_y) + 48 * 4,
			(player.ani[player.state] !== undefined && player.ani[player.state].width 	!== undefined ? player.ani[player.state].width 	: player.ani.default.width),
			(player.ani[player.state] !== undefined && player.ani[player.state].height 	!== undefined ? player.ani[player.state].height : player.ani.default.height)
		);
	}
}

keyHandler	= function() { // keyHandler "class", handles the keys. Would've been better off inside the game class itself after all
	var direction		= "";
	var keys			= new Object();

	this.getDirection	= function() {
		return direction;
	}

	this.keys		= function() {
		return keys;
	}

	document.onkeydown = function(event) {
		keys[event.which] = true;
		
		switch(event.which) {
			case 87: 	// W
				direction = 'n';
				break;
			case 65: 	// A
				direction = 'w';
				break;
			case 83: 	// S
				direction = 's';
				break;
			case 68: 	// D
				direction = 'e';
				break;
		}
	}

	document.onkeyup = function(event) {
		keys[event.which] = undefined;

		switch(event.which) {
			case 87: 	// W
			case 65: 	// A
			case 83: 	// S
			case 68: 	// D
				if(keys[87] == undefined && keys[65] == undefined && keys[83] == undefined && keys[68] == undefined) {
					direction = '';
				} else {
					if(keys[87] != undefined) direction = 'n';
					if(keys[65] != undefined) direction = 'w';
					if(keys[83] != undefined) direction = 's';
					if(keys[68] != undefined) direction = 'e';
				}
				break;
		}
	}

	document.onblur = function(event) {
		keys = new Object;
		direction = "";
	}
}