<?
App::uses('AppModel', 'Model');

class Map extends AppModel {
	var $hasMany = array('Tile' => array(
							'order' => array(
								'Tile.y',
								'Tile.x',
								'Tile.id'
							)
						));
}