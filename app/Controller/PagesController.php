<?
App::uses('AppController', 'Controller');
App::uses('Map', 'Model');
App::uses('Tile', 'Model');
App::uses('File', 'Utility');

class PagesController extends AppController {
	public function display($mapId = null) {
		$this->set('mapId', $mapId ? $mapId : 'null');
	}

	public function editor($mapId = null) {
		if($mapId)
			$this->set('mapId', $mapId);
	}

	public function getMapById($id = null) {
		$Map = new Map;

		if($id && $data = $Map->find('first', array('conditions' => array('Map.id' => $id)))) {
			$mapArr = array();

			foreach($data['Tile'] as $tile) {
				if(!isset($mapArr[$tile['y']]))
					$mapArr[$tile['y']] = array();

				$mapArr[$tile['y']][$tile['x']] = array('x' => $tile['palette_x'] ? $tile['palette_x'] : 0,
														'y' => $tile['palette_y'] ? $tile['palette_y'] : 0,
														'w' => $tile['w'] ? true : false);

				if($tile['interaction'])
					$mapArr[$tile['y']][$tile['x']]['interaction'] = $tile['interaction']; 
			}

			$this->json($mapArr, true);
		} elseif($id == null) { // flowers, grass
			$mapArr = array();
			for($y = -250; $y < 250; $y++) {
				$mapArr[$y] = array();
				for($x = -250; $x < 250; $x++) {
					$mapArr[$y][$x] = array('x' => rand(1, 2) == 2 ? 4 : 1, 'y' =>0);
				}
			}

			$mapArr[1][0] = array('x' => 3, 'y' => 0, 'w' => true, 'interaction' => 'Auto generated map, 500x500');

			$this->json($mapArr, true);
		}

		$this->json(false, false);
	}

	public function saveMap($mapId = null) {
		if($this->data && $this->data['Map']) {
			$Map	= new Map;
			$Tile	= new Tile;

			$map = json_decode($this->data['Map'], true);
			$data = array('Tile' => array(), 'Map' => array('name' => 'testmap'));

			

			foreach($map as $y => $mapRow) {
				foreach($mapRow as $x => $mapColumn) {
					$data['Tile'][] = array('y'				=> $y,
											'x' 			=> $x,
											'palette_x'		=> $mapColumn['x'],
											'palette_y'		=> $mapColumn['y'],
											'w'				=> $mapColumn['w'] == true ? 1 : 0,
											'interaction'	=> isset($mapColumn['interaction']) && !empty($mapColumn['interaction']) ? 
																$mapColumn['interaction'] : NULL
											);
				}
			}

			if($mapId && count($data['Tile']) > 0 && $map = $Map->find('first', array('conditions' => array('Map.id' => $mapId), 'recursive' => -1))) {
				$Tile->deleteAll(array('Tile.map_id' => $mapId));
				$data['Map'] = $map['Map'];
			}

			if(count($data['Tile']) > 0 && $Map->saveAll($data))
				$this->json(array('map_id' => $Map->id), true);
		}

		$this->json(false, false);
	}

	public function getPlayerAnimation() { // Returns player anyway
		$file = new File(WWW_ROOT . 'resources/ani/player.json', true, 0644);
		$this->json(json_decode($file->read()), true);
	}
}