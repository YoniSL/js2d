<?=$this->Html->css('style', null, array('inline' => false));?>
<?=$this->Html->script('editor/core', array('inline' => false));?>
<?=$this->Html->scriptBlock('
	window.onload = function() {
		e = new editor();
		e.init(Object({\'saveBtn\' : \'save-btn\''. (isset($mapId) ? ', \'mapId\' : ' . $mapId : '') . '}));
	};
');?>
<div id='canvas' class='editor'>
	<canvas id='background' width='720px' height='432px'></canvas>
	<div class='palette-holder'>
		<canvas id='palette' width='384px'></canvas>
	</div>
</div>
<center>
	<input type='submit' value='opslaan' id='save-btn' />
</center>