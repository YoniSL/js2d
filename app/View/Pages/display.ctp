<?=$this->Html->css('style', null, array('inline' => false));?>
<?=$this->Html->script('game/core', array('inline' => false));?>
<?=$this->Html->scriptBlock('
	window.onload = function() {
		g = new game();
		g.init(Object({\'fps\' : 50' . ($mapId ? ', \'mapId\' : ' . $mapId : '') . '}));
	};
');?>
<div id='canvas'>
	<canvas id='background' width='720px' height='432px'></canvas>
	<canvas id='entities' width='720px' height='432px'></canvas>
	<canvas id='foreground' width='720px' height='432px'></canvas>
</div>
<? if($mapId) { ?>
	<center>
		<input type='submit' value='Aanpassen' id='save-btn' onclick='window.location = "/editor/<?=$mapId;?>"'/>
	</center>
<? } ?>